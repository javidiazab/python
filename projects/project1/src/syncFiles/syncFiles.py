import filecmp
import os
import shutil
import tkinter as tk
from tkinter import filedialog


def comparar_directorios(dir1, dir2):
    # Comparar los directorios recursivamente
    comparador = filecmp.dircmp(dir1, dir2)

    # Mostrar diferencias
    print("Archivos únicos en", dir1, ":", comparador.left_only)
    print("Archivos únicos en", dir2, ":", comparador.right_only)
    print("Archivos diferentes:", comparador.diff_files)

    # Sincronizar directorios
    opcion = input("¿Desea sincronizar los directorios? (S/N): ").upper()
    if opcion == 'S':
        copiar_archivos(dir1, dir2, comparador.left_only)
        copiar_archivos(dir2, dir1, comparador.right_only)


def copiar_archivos(origen, destino, archivos):
    for archivo in archivos:
        origen_archivo = os.path.join(origen, archivo)
        destino_archivo = os.path.join(destino, archivo)
        if os.path.isdir(origen_archivo):
            shutil.copytree(origen_archivo, destino_archivo)
        else:
            shutil.copy2(origen_archivo, destino_archivo)
            print("Se copió", origen_archivo, "a", destino_archivo)


def seleccionar_directorio(label):
    directorio = filedialog.askdirectory()
    label.config(text=directorio)


if __name__ == "__main__":
    root = tk.Tk()
    root.title("Comparador de Directorios")

    # Crear etiquetas y botones para seleccionar directorios
    label1 = tk.Label(root, text="Directorio 1:")
    label1.grid(row=0, column=0)
    seleccion1 = tk.Label(root, text="")
    seleccion1.grid(row=0, column=1)
    boton1 = tk.Button(root, text="Seleccionar", command=lambda: seleccionar_directorio(seleccion1))
    boton1.grid(row=0, column=2)

    label2 = tk.Label(root, text="Directorio 2:")
    label2.grid(row=1, column=0)
    seleccion2 = tk.Label(root, text="")
    seleccion2.grid(row=1, column=1)
    boton2 = tk.Button(root, text="Seleccionar", command=lambda: seleccionar_directorio(seleccion2))
    boton2.grid(row=1, column=2)

    # Botón para iniciar la comparación de directorios
    boton_comparar = tk.Button(root, text="Comparar Directorios",
                               command=lambda: comparar_directorios(seleccion1.cget("text"), seleccion2.cget("text")))
    boton_comparar.grid(row=2, columnspan=3)

    root.mainloop()