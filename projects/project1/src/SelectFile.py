import tkinter as tk
from tkinter import filedialog

# Crear una ventana de tkinter
root = tk.Tk()
root.withdraw()  # Ocultar la ventana principal

# Obtener el directorio actual
directorio_actual = "./"  # Puedes cambiar esto según tu directorio actual

# Mostrar el cuadro de diálogo para seleccionar un archivo .java
ruta_archivo = filedialog.askopenfilename(initialdir=directorio_actual, title="Seleccionar archivo .java", filetypes=(("Archivos Java", "*.java"), ("Todos los archivos", "*.*")))

# Mostrar la ruta del archivo seleccionado
print("Archivo seleccionado:", ruta_archivo)
