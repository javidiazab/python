import tkinter as tk
from tkinter import filedialog
import os

def obtener_archivos_java(ruta_directorio):
    archivos_java = []
    for raiz, directorios, archivos in os.walk(ruta_directorio):
        for archivo in archivos:
            if archivo.endswith(".java"):
                archivos_java.append(os.path.join(raiz, archivo))
    return archivos_java

# Crear una ventana de tkinter
root = tk.Tk()
root.withdraw()  # Ocultar la ventana principal

# Mostrar el cuadro de diálogo para seleccionar un directorio
directorio = filedialog.askdirectory(title="Seleccionar directorio")

# Obtener la lista de archivos Java dentro del directorio y sus subdirectorios
archivos_java = obtener_archivos_java(directorio)

# Mostrar la lista de archivos Java encontrados
print("Archivos Java encontrados:")
for archivo in archivos_java:
    print(archivo)
