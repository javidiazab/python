import javalang

# Cargar el archivo Java
with open("Clase2.java", "r") as archivo:
    codigo_java = archivo.read()

# Analizar el código Java
arbol_sintactico = javalang.parse.parse(codigo_java)

# Ejemplo: Obtener información sobre las clases
for nodo in arbol_sintactico.types:
    if isinstance(nodo, javalang.tree.ClassDeclaration):
        print("Nombre de la clase:", nodo.name)
        print("Métodos de la clase:")
        for metodo in nodo.methods:
            print("- Nombre:", metodo.name)
            print("- Parámetros:", metodo.parameters)