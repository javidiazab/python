package com.pagonxt.paymentshub.iberpayinstgateway.service.inbound;

import static com.pagonxt.paymentshub.iberpayinstgateway.model.MessageReason.DUPLICATED;
import static com.pagonxt.paymentshub.iberpayinstgateway.model.MessageReason.FATAL_EXCEPTION;
import static com.pagonxt.paymentshub.iberpayinstgateway.model.MessageReason.ORIGINAL_NOT_FOUND;
import static com.pagonxt.paymentshub.iberpayinstgateway.model.MessageReason.SCHEME_REJECTED;
import static com.pagonxt.paymentshub.iberpayinstgateway.model.MessageStatus.ERROR;
import static com.pagonxt.paymentshub.iberpayinstgateway.model.MessageStatus.PROCESSED;
import static com.pagonxt.paymentshub.iberpayinstgateway.model.MessageStatus.RECEIVED;
import static com.pagonxt.paymentshub.iberpayinstgateway.model.MessageStatus.REJECTED;
import static com.pagonxt.paymentshub.iberpayinstgateway.model.iberpay.IberpayStatusCode.IBP200;
import static com.pagonxt.paymentshub.iberpayinstgateway.utils.ComponentUtils.updateRoutingKey;
import static com.pagonxt.paymentshub.iberpayinstgateway.utils.iberpay.IberpayStatusReportUtils.getIberpayPacs002;
import static com.pagonxt.paymentshub.iberpayinstgateway.utils.iberpay.converter.IberpayReturnConverter.getPacs004Flavoured;
import static com.pagonxt.paymentshub.iberpayinstgateway.utils.ph.PhMessageUtils.buildExecutionMessage;
import static com.pagonxt.paymentshub.iberpayinstgateway.utils.ph.PhMessageUtils.getExternalCode;
import static com.pagonxt.paymentshub.sepainstlib.utils.Shortname.camt_056;
import static com.pagonxt.paymentshub.sepainstlib.utils.Shortname.pacs_002;
import static com.pagonxt.paymentshub.sepainstlib.utils.Shortname.pacs_004;
import static com.pagonxt.paymentshub.sepainstlib.utils.Shortname.pacs_008;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static uk.co.santander.iso20022.core.TxStatus.RJCT;
import static uk.co.santander.iso20022.core.type.reason.StatusReason.StatusReasonCodes.NARR;
import static uk.co.santander.iso20022.messaging.routingkey.ProcessStatus.ko;
import static uk.co.santander.iso20022.messaging.routingkey.ProcessStatus.ok;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pagonxt.paymentshub.iberpayinstgateway.components.FeignClientComponent;
import com.pagonxt.paymentshub.iberpayinstgateway.components.IberpayUtilsComponent;
import com.pagonxt.paymentshub.iberpayinstgateway.components.QueueComponent;
import com.pagonxt.paymentshub.iberpayinstgateway.config.RulebookConfig;
import com.pagonxt.paymentshub.iberpayinstgateway.exceptions.DatabaseMessageNotFoundException;
import com.pagonxt.paymentshub.iberpayinstgateway.exceptions.InvalidMessageException;
import com.pagonxt.paymentshub.iberpayinstgateway.message.EventEmitter;
import com.pagonxt.paymentshub.iberpayinstgateway.model.MessageStatus;
import com.pagonxt.paymentshub.iberpayinstgateway.model.SettlementMethodRegister;
import com.pagonxt.paymentshub.iberpayinstgateway.model.request.IberpayRequest;
import com.pagonxt.paymentshub.iberpayinstgateway.repository.MessageEntity;
import com.pagonxt.paymentshub.iberpayinstgateway.repository.MessageFilter;
import com.pagonxt.paymentshub.iberpayinstgateway.repository.MessageRepository;
import com.pagonxt.paymentshub.iberpayinstgateway.service.common.MessageService;
import com.pagonxt.paymentshub.iberpayinstgateway.utils.iberpay.IberpayStatusReportUtils;
import com.pagonxt.paymentshub.iberpayinstgateway.utils.iberpay.IberpayUtils;
import com.pagonxt.paymentshub.iberpayinstgateway.utils.ph.PhMessageUtils;
import com.pagonxt.paymentshub.sepainstlib.model.SepaInstMessage;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import uk.co.santander.iso20022.core.Flow;
import uk.co.santander.iso20022.core.MessageDefinition;
import uk.co.santander.iso20022.core.PaymentContextHubMessage;
import uk.co.santander.iso20022.core.ReasonInfo;
import uk.co.santander.iso20022.core.ReasonInfo.AdditionalInformation;
import uk.co.santander.iso20022.core.reports.SchemeReport;
import uk.co.santander.iso20022.core.type.reason.StatusReason;
import uk.co.santander.iso20022.core.util.CoreUtil;
import uk.co.santander.iso20022.messaging.PaymentContextExecutionMessage;
import uk.co.santander.iso20022.messaging.routingkey.RoutingKey;
import uk.co.santander.isocoreclient.api.IsoCoreApi;

@Slf4j
@Service
@RequiredArgsConstructor
public class ReturnInboundService {

    private final MessageRepository messageRepository;
    private final MessageService messageService;
    private final IberpayUtilsComponent iberpayUtilsComponent;
    private final IsoCoreApi isoCoreApi;
    private final EventEmitter eventEmitter;
    private final FeignClientComponent feignClientComponent;
    private final QueueComponent queueComponent;
    private final ObjectMapper objectMapper;
    private final RulebookConfig rulebookConfig;

    @Value("${app.switch-flow}")
    private List<String> clientSanEs;

    public void launchProcess(PaymentContextExecutionMessage executionMessage, RoutingKey routingKey) {
        val message = MessageEntity.builder()
            .status(RECEIVED)
            .startTime(LocalDateTime.now())
            .build();

        try {
            process(message, executionMessage, routingKey);
        } catch (DatabaseMessageNotFoundException e) {
            log.info("Original message not found: {}", e.getMessage(), e);
            message.updateStatus(REJECTED, ORIGINAL_NOT_FOUND, ORIGINAL_NOT_FOUND.desc());
            saveMessage(message);
        } catch (Exception e) {
            log.error("Unexpected exception while processing message: {}", e.getMessage(), e);
            message.updateStatus(ERROR, FATAL_EXCEPTION, FATAL_EXCEPTION.desc() + ":" + e.getMessage());
            saveMessage(message);
            throw e;
        }
    }

    private void process(MessageEntity returnMessage, PaymentContextExecutionMessage executionMessage, RoutingKey routingKey) {
        val returnHubMessage = executionMessage.getHubMessage();

        fillInitialFields(returnHubMessage, returnMessage);
        val paymentHubId = returnMessage.getPaymentHubId();
        val clientId = returnMessage.getClientId();

        if (isDuplicated(paymentHubId)) {
            log.warn("[{}] Duplicated returnMessage received", paymentHubId);
            returnMessage.updateStatus(ERROR, DUPLICATED, DUPLICATED.desc());
            saveMessage(returnMessage);
            return;
        }

        val originalPayment = findOriginalPayment(returnMessage);

        val recallPayment = findRecallPayment(returnMessage);

        val iberpayReturnXml = mapToSepaInstVersion(returnHubMessage, returnMessage, originalPayment, recallPayment);

        if (clientSanEs.contains(clientId)) {
            log.info("[{}] Send to ep gateway to save reference", paymentHubId);
            queueComponent.sendToEpGatewayReference(executionMessage);
        }
        SepaInstMessage<?> schemeResponse;

        try {
            // Trying to prevent instant confirmation of happy path
            saveMessage(returnMessage);

            schemeResponse = sendToScheme(returnMessage, paymentHubId, clientId, iberpayReturnXml);
        } catch (InvalidMessageException exception) {
            log.info("[{}] Received Pacs002 ko from iberpay without document", paymentHubId);
            returnMessage.updateStatus(REJECTED, SCHEME_REJECTED, SCHEME_REJECTED.desc());
            saveMessage(returnMessage);

            val statusReportHubMessage = returnHubMessage.asRequest().statusReportFactory().create(SchemeReport.builder()
                .txStatus(RJCT)
                .reasonInfo(ReasonInfo.builder()
                    .code(StatusReason.code(NARR))
                    .rawCode(NARR.code())
                    .rawDescription(NARR.definition())
                    .additionalInformation(
                        AdditionalInformation.additionalInformation().add("Scheme returns a not valid response"))
                    .build())
                .build());

            val newRoutingKey = updateRoutingKey(routingKey, statusReportHubMessage.definition(), ko);
            isoCoreApi.finaliseExecution(buildExecutionMessage(statusReportHubMessage), newRoutingKey);
            return;
        }

        val status = IberpayStatusReportUtils.getTxStatus(schemeResponse).orElseThrow();
        val statusReasonCode = IberpayStatusReportUtils.getStatusReasonCode(schemeResponse);
        val statusReason = getExternalCode(statusReasonCode.orElse(null));
        val statusReportHubMessage = returnHubMessage.asRequest()
            .statusReportFactory()
            .create(SchemeReport.builder()
                .txStatus(status)
                .reasonInfo(setupReasonInfo(statusReason.orElse(null)))
                .build());

        val isRejected = RJCT == status;
        val newRoutingKey = updateRoutingKey(routingKey, statusReportHubMessage.definition(), isRejected ? ko : ok);

        if (RJCT == status) {
            returnMessage.updateStatus(REJECTED, SCHEME_REJECTED, SCHEME_REJECTED.desc());
            saveMessage(returnMessage);

            if (clientSanEs.contains(clientId)) {
                isoCoreApi.finaliseExecution(buildExecutionMessage(statusReportHubMessage), newRoutingKey);
            }
        } else {
            if (clientSanEs.contains(clientId)) {
                log.info("[{}] Sending to ep gateway settlement method", paymentHubId);
                sendToEpRegistration(paymentHubId, schemeResponse);
            }

            queueComponent.sendToExecutionUpdate(paymentHubId, statusReportHubMessage, newRoutingKey.toString());
        }
    }

    private static ReasonInfo setupReasonInfo(StatusReason statusReason) {
        if (isNull(statusReason)) {
            return null;
        }

        val rawDescription = statusReason.isCode()
            ? statusReason.isoCode().definition()
            : statusReason.value();

        return ReasonInfo.builder()
            .code(statusReason)
            .rawCode(statusReason.value())
            .rawDescription(rawDescription)
            .build();
    }

    private SepaInstMessage<?> sendToScheme(MessageEntity returnMessage, String paymentHubId, String clientId,
        String iberpayReturnXml) {
        eventEmitter.emitSentEvent(paymentHubId, iberpayReturnXml, returnMessage.getExtMessageType().schema(), clientId);
        log.info("[{}] Sending to iberpay", paymentHubId);

        val iberpayRequest = IberpayRequest.builder()
            .document(iberpayReturnXml.getBytes(StandardCharsets.UTF_8))
            .build();
        val iberpayResponse = feignClientComponent.sendRefund(iberpayRequest, clientId);

        log.info("[{}] Response from iberpay {}", paymentHubId, iberpayResponse.getStatus());

        try {
            val eventPayload = new HashMap<String, String>();
            eventPayload.put("status", iberpayResponse.getStatus().name());

            if (nonNull(iberpayResponse.getDocument())) {
                eventPayload.put("Document", new String(iberpayResponse.getDocument(), StandardCharsets.UTF_8));
            }

            eventEmitter.emitSchemeResponseEvent(paymentHubId,
                objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(eventPayload),
                rulebookConfig.iberpay(pacs_002), clientId);
        } catch (JsonProcessingException e) {
            log.error("Unable to launch scheme response event: {}", e.getMessage(), e);
        }

        if (IBP200 != iberpayResponse.getStatus() || isNull(iberpayResponse.getDocument())) {
            throw new InvalidMessageException("Got empty/invalid response from iberpay");
        }

        return getIberpayPacs002(iberpayResponse);
    }

    private void sendToEpRegistration(String paymentHubId, SepaInstMessage<?> iberpayStatusReport) {
        log.info("[{}] Send to ep gateway settlement method", paymentHubId);
        val settlementMethodRegister = SettlementMethodRegister.builder()
            .settlementMethod(IberpayUtils.getMessageId(iberpayStatusReport).orElseThrow()
                .substring(11, 14))
            .paymentsHubId(paymentHubId)
            .build();
        queueComponent.sendToEpGatewaySettlementMethod(settlementMethodRegister);
    }

    private MessageEntity findOriginalPayment(MessageEntity returnMessage) {
        val phOriginalMsgId = returnMessage.getPhOriginalMsgId();
        val originalPaymentHubId = CoreUtil.generatePaymentHubId(phOriginalMsgId, returnMessage.getClientId()).toString();

        return messageService.find(MessageFilter.builder()
            .paymentHubId(originalPaymentHubId)
            .messageType(pacs_008)
            .statusNot(MessageStatus.ERROR)
            .build());
    }

    private PaymentContextHubMessage<?> findRecallPayment(MessageEntity returnMessage) {
        final MessageEntity originalRecallMessage = messageService.find(MessageFilter.builder()
            .originalReferenceId(returnMessage.getOriginalReferenceId())
            .referenceAgent(returnMessage.getReferenceAgent())
            .messageType(camt_056)
            .flow(Flow.reception)
            .status(PROCESSED)
            .build());

        return isoCoreApi.fetchRequestHubMessage(UUID.fromString(originalRecallMessage.getPaymentHubId()));
    }

    private void fillInitialFields(PaymentContextHubMessage<?> hubMessage, MessageEntity message) {
        message.setPaymentHubId(hubMessage.paymentHubId().toString());
        message.setPhMessageId(PhMessageUtils.getMessageId(hubMessage).orElseThrow());
        message.setPhMessageType(hubMessage.definition());
        message.setPhOriginalInstructionId(PhMessageUtils.getInstructionId(hubMessage).orElse(null));
        message.setPhOriginalMsgId(PhMessageUtils.getOriginalMessageId(hubMessage.asRequest())
            .orElseThrow(() -> new InvalidMessageException("Unable to retrieve original message id from given message")));

        // common fields
        message.setReferenceId(PhMessageUtils.getTransactionId(hubMessage).orElse(null));
        message.setMessageType(pacs_004);
        message.setOriginalReferenceId(PhMessageUtils.getOriginalTransactionId(hubMessage).orElse(null));
        message.setFlow(hubMessage.flow());
        message.setClientId(hubMessage.client().getInternalId());
        // At this point fill reference agent with creditor bic (which will be pacs004 instructing agent bic)
        message.setReferenceAgent(PhMessageUtils.getDebtorBic(hubMessage).orElse(null));
    }

    private boolean isDuplicated(String paymentHubId) {
        return messageRepository.existsByPaymentHubIdAndMessageTypeAndStatusNot(paymentHubId, pacs_004, ERROR);
    }

    private String mapToSepaInstVersion(PaymentContextHubMessage<?> returnHubMessage,
        MessageEntity returnMessage, MessageEntity originalMessage, PaymentContextHubMessage<?> recallPayment) {

        final MessageDefinition definition = rulebookConfig.iberpay(pacs_004);

        val extMessageId = iberpayUtilsComponent.getNewMessageId(definition);
        returnMessage.setExtMessageId(extMessageId);
        returnMessage.setExtMessageType(definition);
        returnMessage.setExtOriginalMsgId(originalMessage.getExtMessageId());
        returnMessage.setExtOriginalInstructionId(originalMessage.getExtInstructionId());
        val pacs04 = getPacs004Flavoured(returnHubMessage, returnMessage, definition, recallPayment, originalMessage);

        val pacs004Xml = pacs04.toXml();
        returnMessage.setExtPayload(pacs004Xml);

        return pacs004Xml;
    }

    private MessageEntity saveMessage(MessageEntity message) {
        message.setEndTime(LocalDateTime.now());
        return messageRepository.save(message);
    }
}
