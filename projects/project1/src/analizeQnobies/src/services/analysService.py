import tkinter as tk
from projects.project1.src.analizeQnobies.src.model.qnobiClasses import Flow
from projects.project1.src.analizeQnobies.src.model.qnobiClasses import Step
from projects.project1.src.analizeQnobies.src.model.qnobiClasses import Environment
from projects.project1.src.analizeQnobies.src.model.qnobiClasses import Collection
from projects.project1.src.analizeQnobies.src.model.qnobiClasses import E2ERepository
import os

csv_char = ';'
excluded_dirs = ["inbound-coexistences-sande-collection", "outbound-coexistences-epgw-collection",
                 "outbound-coexistences-sande-collection"]
excluded_files = ["request.json", "test.js", "prerequest.js", ".qnobi.json", "qnobi.json", "data-file.hjson",
                  "data-file.json"]
excluded_flows = []
results = []


def read_file(file):
    file = open(file, 'r')
    body = file.read()
    file.close()
    return body


def save_qnobies(squad, E2ERepository, csv_char, selected_environment, output_file):
    headings = ("Repo" + csv_char + "Collection" + csv_char + "Flow" + csv_char + "Environment" + csv_char + "Squad" + csv_char
                     + "Collection" + csv_char +
                     "Flow" + csv_char + "E2E test" + csv_char + "Step" + csv_char + "Checks")
    previous_collection = ""
    output_file.write(headings + '\n')
    for collection in E2ERepository.get_collections():

        for flow in collection.get_flows():
            leading = "{}{}{}{}{}{}{}{}{}".format(E2ERepository.get_name(), csv_char, flow.get_collection(), csv_char, flow.get_name(), csv_char,
                                                selected_environment, csv_char, squad, csv_char)
            if flow.find_environment(selected_environment) and flow.get_name() not in excluded_flows:
                # if collection not in excluded_dirs:
                if previous_collection != flow.get_collection():
                    previous_collection = flow.get_collection()
                    line_collection = "{}{}{}{}{}{}{}{}".format(E2ERepository.get_name(), csv_char, flow.get_collection(), csv_char * 2,
                                                            csv_char, squad, csv_char, flow.get_collection())
                    print(line_collection)
                    output_file.write(line_collection + '\n')
                line_flow = "{}{}{}{}{}{}".format(leading, csv_char, csv_char, flow.get_name(), csv_char, "YES")
                print(line_flow)
                output_file.write(line_flow + '\n')
                # line_environment = "{}{}{}".format(leading, csv_char * 2, selected_environment)
                # print(line_environment)
                # output_file.write(line_environment + '\n')
                for step in flow.get_steps():
                    step_details = "Calls to '" + step.get_request_call() + "'"
                    if step.get_test_checks_final_status():
                        step_details += " and verifies final status is " + step.get_test_checks_final_status()
                    if step.get_test_checks_expected_events(flow.find_environment(selected_environment)):
                        step_details += (" and verifies " + step.get_test_checks_expected_events(
                            flow.find_environment(selected_environment)).replace('": "', "' is '")
                                         .replace('"', "'").replace('\n', '').replace('        ', ', '))
                    if step.get_test_checks_audit_status():
                        step_details += " and verifies status in DB is " + step.get_test_checks_audit_status()
                    if step.get_test_checks_response_status():
                        step_details += " and verifies status is " + step.get_test_checks_response_status()
                    if "and verifies" not in step_details:
                        step_details += " and verifies that return status is OK"
                    line_step = "{}{}{}{}{}".format(leading, csv_char * 3, step.get_name(), csv_char, step_details)
                    print(line_step)
                    output_file.write(line_step + '\n')


def list_directories_and_files(start_path, depth=0):
    print('  ' * depth + '|---' + os.path.basename(start_path))
    for file_name in os.listdir(start_path):
        full_path = os.path.join(start_path, file_name)
        if os.path.isfile(full_path):
            if file_name not in excluded_files:
                print('  ' * (depth + 1) + '|---' + file_name)
        elif os.path.isdir(full_path):
            if full_path not in excluded_dirs:
                list_directories_and_files(full_path, depth + 1)


def list_directories_and_files_excel2(start_path, depth=0):
    # print(csv_char * depth + os.path.basename(start_path))
    results.append(csv_char * depth + os.path.basename(start_path))
    for file_name in os.listdir(start_path):
        full_path = os.path.join(start_path, file_name)
        if os.path.isfile(full_path):
            if file_name not in excluded_files:
                # print(csv_char * (depth + 1) + '|---' + file_name)
                results.append(csv_char * (depth + 1) + file_name)
        elif os.path.isdir(full_path):
            if file_name not in excluded_dirs:
                list_directories_and_files_excel2(full_path, depth + 1)


def load_repository_with_collections(repo):
    collections_path = os.path.join(repo.get_path(), "collections")
    print("Start processing collections at", collections_path)

    for collection in os.listdir(collections_path):  # collections
        collection_path = os.path.join(collections_path, collection)
        if os.path.isdir(collection_path):  # collection
            current_collection = Collection(collection_path)
            repo.add_collection(current_collection)
            print("Collection =", current_collection.get_name())

    sorted(repo.get_collections(), key=lambda col: col.name)  # ordena los flows
    return repo


def load_collections_with_flows(repo):
    for collection in repo.get_collections():
        print("Tratando coleccion: ", collection.get_name())
        load_flow_data(collection, repo)   # Quitar repo, solo en pruebas
        load_flow_src(collection)
        # sorted(collection.get_flows(), key=lambda flow: flow.name())
    return repo


def load_flow_data(collection, repo):
    new_flows = []
    for data_flow in os.listdir(collection.get_data_path()):  # data/flow
        data_path = os.path.join(collection.get_data_path(), data_flow)
        if os.path.isdir(data_path):
            new_flow = Flow(data_flow, collection.get_name())
            print("   Tratando data-flow: ", new_flow.get_name())
            new_flow.set_data_path(data_path)
            collection.add_flow(new_flow)
            new_flows.append(new_flow)
            for environment in os.listdir(data_path):  # data/flow/environments
                environment_path = os.path.join(data_path, environment)
                if os.path.isdir(environment_path):
                    print("      tratando environment ->", environment)
                    new_environment = Environment(environment)
                    new_flow.add_environment(new_environment)
                    for data_file in os.listdir(environment_path):  # data/flow/environment/data_file
                        data_file_path = os.path.join(environment_path, data_file)
                        if os.path.isfile(data_file_path):
                            new_environment.set_file_data_name(data_file_path)
                            new_environment.set_file_data_body(read_file(data_file_path))
    collection.add_flows(new_flows)


def load_flow_src(collection):
    print("aqui",collection.get_name())
    for src_flow in os.listdir(collection.get_src_path()):  # src/flow
        print("   Tratando  src_flow: ", src_flow)
        src_path = os.path.join(collection.get_src_path(), src_flow)
        if os.path.isdir(src_path):
            # Busca en la lista de flujos un flujo cuyo nombre sea igual a src_flow
            flow = next((flow for flow in collection.get_flows() if flow.name == src_flow), None)
            if flow:
                flow.set_src_path(src_path)
                steps = []
                for step in os.listdir(src_path):  # /src/flow/step
                    step_path = os.path.join(src_path, step)
                    if os.path.isdir(step_path):
                        print("      Found step ->", step)
                        new_step = Step(step)
                        steps.append(new_step)
                        # flow.add_step(new_step)
                        for step_file in os.listdir(step_path):  # src/flow/step/prerequest.js
                            load_js_files(new_step, step_file, step_path)

                steps.sort(key=lambda steps: steps.name)
                flow.set_steps(steps)
                # flow.get_steps().sort(key=lambda steps: steps.name)

def load_js_files(new_step, step_file, step_path):
    step_file_path = os.path.join(step_path, step_file)
    if os.path.isfile(step_file_path):
        if os.path.basename(step_file_path) == "prerequest.js":
            new_step.set_prerequest(read_file(step_file_path))
        if os.path.basename(step_file_path) == "request.json":
            new_step.set_request(read_file(step_file_path))
        if os.path.basename(step_file_path) == "test.js":
            new_step.set_test(read_file(step_file_path))


def save_qnobies_in_file(repo, selected_environment, csv_char):
    with open("../../output/" + repo.get_name() + ".txt", 'w') as output_file:
        save_qnobies(repo.get_owner(), repo, csv_char, selected_environment, output_file)
    output_file.close()


def print_qnobies(repo):
    csv_char = "    "
    previous_collection = ""
    for collection in E2ERepository.get_collections():
        for flow in collection.get_flows():
            if selected_environment in flow.get_environments():
                if previous_collection != flow.get_collection():
                    previous_collection = flow.get_collection()
                    print("{}".format(flow.get_collection()))
                print(csv_char * 1 + "{}".format(flow.get_name()))
                print(csv_char * 2 + "{}".format(selected_environment))
                for step in flow.get_steps():
                    name = step.get_name()
                    print(csv_char * 3 + "{}".format(step.get_name()))


def load_repository(repository):
    load_repository_with_collections(repository)
    load_collections_with_flows(repository)


if __name__ == "__main__":
    root = tk.Tk()
    root.withdraw()

    # Mostrar el cuadro de diálogo para seleccionar un directorio
    # start_directory = filedialog.askdirectory(title="Seleccionar directorio")

    # repo_directory = '/Users/N90578/Library/CloudStorage/OneDrive-SantanderOffice365/Documentos/WksPh/501/e2e-test-collections-iberpay-sepa-inst'
    repo_directory = '/Users/n90578/Library/CloudStorage/OneDrive-SantanderOffice365/Documents/WksPh/501/e2e-test-collections-bizum'
    # repo_directory = '/Users/N90578/Library/CloudStorage/OneDrive-SantanderOffice365/Documentos/WksPh/Wolfpack/e2e-test-collections-instant-payments-orchestrator'
    selected_environment = "k8s-euw1-eu-pre"
    owner = "Vaders-Fist"

    e2e_repo = E2ERepository(repo_directory, owner)
    load_repository(e2e_repo)
    save_qnobies_in_file(e2e_repo, selected_environment, ';')
    # print_qnobies(e2e_repo)
