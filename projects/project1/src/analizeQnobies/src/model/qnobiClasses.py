import os.path
import re

from typing import List
from projects.project1.src.analizeQnobies.src.utils import constants


class Environment:
    name = ""
    file_data_name = ""
    file_data_body = ""

    def __init__(self, environment):
        self.name = environment

    def set_file_data_name(self, value):
        self.file_data_name = value

    def set_file_data_body(self, value):
        self.file_data_body = value

    def get_name(self):
        return self.name

    def get_file_data_name(self):
        return self.file_data_name

    def get_file_data_body(self):
        return self.file_data_body


class Step:
    name = ""
    prerequest = ""
    request = ""
    test = ""

    def __init__(self, step):
        self.name = step

    def set_prerequest(self, value):
        self.prerequest = value

    def set_request(self, value):
        self.request = value

    def set_test(self, value):
        self.test = value

    def get_name(self):
        return self.name

    def get_prerequest(self):
        return self.prerequest

    def get_request(self):
        return self.request

    def get_test(self):
        return self.test

    def get_request_call(self):
        match = re.search(constants.pattern_call_url, self.request)
        if match:
            return match.group(1)
        else:
            match = re.search(constants.pattern_call_host, self.request)
            if match:
                return match.group(1)
            else:
                return None

    def get_test_checks_final_status(self):
        match = re.search(constants.pattern_checks_final_status, self.test)
        if match:
            return match.group(1)
        else:
            return None

    def get_test_checks_expected_events(self, environment):
        match = re.search(constants.pattern_checks_expected_sctinst_events, self.test)   # checks SCT-INST
        if match:
            # events_vble = match.group(1)
            match = re.search(constants.pattern_checks_events_vble, self.test)
            if match:
                events_vble = match.group(1)
                data_file = environment.get_file_data_body()
                match = re.search(constants.pattern_data_file_sctinst_events.replace("XXX-events", events_vble), data_file)
                if match:
                    return match.group(1)
        else:   # checks BIZUM
            match = re.search(constants.pattern_checks_expected_bizum_events, self.test)  # checks SCT-INST
            if match:
                events_vble = match.group(1)
                data_file = environment.get_file_data_body()
                match = re.search(constants.pattern_data_file_bizum_fmt1.replace("XXX-events", events_vble), data_file)
                if match:
                    return match.group(1)
                else:
                    match = re.search(constants.pattern_data_file_bizum_fmt2.replace("XXX-events", events_vble), data_file)
                    if match:
                        return match.group(1).replace("\n", "").replace("{","").replace("}","").strip()

        return None

    def get_test_checks_audit_status(self):
        match = re.search(constants.pattern_checks_audit_status, self.test)
        if match:
            return match.group(1)
        else:
            return None

    def get_test_checks_response_status(self):
        match = re.search(constants.pattern_checks_response_status, self.test)
        if match:
            return match.group(1)
        else:
            return None


class Flow:
    name = ""
    collection = ""
    data_path = ""
    src_path = ""
    data = ""
    src = ""
    environments: List[Environment] = []
    steps: List[Step] = []

    def __init__(self, name, collection):
        self.name = name
        self.collection = collection

        # self.annotations = annotations
        # self.attributes = attributes
        # self.methods = methods
        # self.dependencies = dependencies
        # self.extends = extends
        # self.implements = implements
        # self.body = body
        # self.throws = throws

        # print("Se acaba de crear la clase {} {} {}".format(self.name, self.collection, self.flow))

    def __str__(self):
        pass
        # return "Clase: {}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n".format(
        # self.class_name,
        # self.scope,
        # self.type,
        # self.abstract,
        # self.final,
        # self.annotations,
        # self.attributes,
        # self.methods,
        # self.dependencies,
        # self.depends,
        # self.extends,
        # self.implements,
        # self.throws,
        # self.body,

        # )

    def get_name(self):
        return self.name

    def get_collection(self):
        return self.collection

    def get_environments(self):
        return self.environments

    def get_src_path(self):
        return self.src_path

    def get_steps(self):
        return self.steps

    def add_environment(self, value):
        self.environments.append(value)

    def add_step(self, value):
        self.steps.append(value)

    def set_data_path(self, value):
        self.data_path = value

    def set_src_path(self, value):
        self.src_path = value

    def set_collection(self, value):
        self.collection = value

    def init_steps(self):
        self.steps = []

    def set_steps(self, value):
        self.steps = value

    def find_environment(self, search_environment):
        for environment in self.environments:
            if environment.get_name() == search_environment:
                return environment
        return None


class Collection:
    name = ""
    path = ""
    flows: List[Flow] = []
    data_path = ""
    src_path = ""

    def __init__(self, path):
        self.name = os.path.basename(path)
        self.path = path
        self.data_path = os.path.join(path, "data")
        self.src_path = os.path.join(path, "src")

    def get_name(self):
        return self.name

    def get_path(self):
        return self.path

    def get_flows(self):
        return self.flows

    def get_data_path(self):
        return self.data_path

    def get_src_path(self):
        return self.src_path

    def add_flow(self, value):
        self.flows.append(value)

    def add_flows(self, value):
        self.flows = value


class E2ERepository:
    name = ""
    path = ""
    owner = ""
    collections: List[Collection] = []

    def __init__(self, repository, owner):
        self.name = os.path.basename(repository)
        self.path = repository
        self.owner = owner

    def get_name(self):
        return self.name

    def get_owner(self):
        return self.owner

    def get_path(self):
        return self.path

    def get_collections(self):
        return self.collections

    def add_collection(self, value):
        self.collections.append(value)

