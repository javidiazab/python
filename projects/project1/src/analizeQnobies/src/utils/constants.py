pattern_call_url = r'"url":.*\{\s*"raw":\s*"([^"]+)"'
pattern_call_host = r'"url":.*\{\s*"host":\s*\[\s*"([^"]+)"'
pattern_checks_final_status = r'checkIfStatusIsOk\(([^)]+)\)'
pattern_checks_expected_sctinst_events = r'includesExpectedEventList\(expectedEvents\)'
pattern_checks_expected_bizum_events = r"E2E.environment.getVariable\('expectations'\)\['([^']+)'\]"
pattern_checks_events_vble = r"const expectedEvents = E2E.environment.getVariable\('([^']+)'\)"
pattern_data_file_sctinst_events = r'"XXX-events"\s*:\s*("[^"]+")'
pattern_data_file_bizum_fmt1 = r'"XXX-events":\s*[{]\s*([^\]]+)\s*[}]'
pattern_data_file_bizum_fmt2 = r'"XXX-events":\s*[\[]\s*([^\]]+)\s*[\]]'
pattern_checks_audit_status = r'pm.expect\(audit\?.status\).to.eql\(([^)]+)\)'
pattern_checks_response_status = r'pm.expect\(status\).to.eql\(([^)]+)\)'