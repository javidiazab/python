import tkinter as tk
from tkinter import filedialog
from tkinter import simpledialog
from tkinter import messagebox
import re
import os

from projects.project1.src.javaComments.src.model.commentClasses import Comment
from projects.project1.src.javaComments.src.model.commentClasses import MyFile
from projects.project1.src.javaComments.src.model.commentClasses import Project

from projects.project1.src.javaComments.src.utils import constants

initial_dir = '~'


def read_file(file):
    try:
        with open(file, 'r') as file:
            return file.readlines()
    except:
        return None


def write_file(file, text):
    with open(file, 'w') as file:
        file.writelines(text)


def save_project_to_file(project):
    with open("../../output/" + project.get_name() + ".txt", 'w') as output_file:
        print("Imprimiendo comentarios")
        output_file.write("0 - " + project.get_path() + '\n')
        for file in project.get_files():
            output_file.write("1 - " + file.get_path() + '\n')
            print("|___ {0} - {1}".format(file.get_name(), file.get_path()))
            for comment in file.get_comments():
                output_file.write("2 - " + str(comment.get_line_number()) + '\n')
                output_file.write("2 - " + comment.get_line())
                print("   |____", str(comment.get_line()).replace("\n", ""))
        output_file.write("3 - " + "EOF")


def load_project_from_file(path):
    lines = read_file(path)
    if lines is None:
        return None

    it = iter(lines)
    line = next(it)
    project = Project(line[4:-1])
    line = next(it)
    while line != "3 - EOF":
        file = MyFile(line[4:-1])
        comments = []
        line = next(it)
        while line.startswith("2"):
            line_number = line
            line = next(it)
            comment = Comment(line[4:], line_number[4:-1])
            comments.append(comment)
            line = next(it)
        file.set_comments(comments)
        project.add_file(file)
    return project


def find_and_remove_comments(project, files):
    for file in files:
        lines = read_file(file)
        if lines is None:
            return None

        lines_counter = 0
        number_lines_substracted = 0
        comments = []
        #for line in lines:
        while lines_counter < len(lines): # utilizamos un while en vez de un for porque en un momento dado quremos retroceder 1 pos y con el for no se puede
            line = lines[lines_counter]
            if is_comment_line(line):
                if get_line_without_comment(line) == '\n':  # si es solo una linea de comentarios sin codigo
                    comments.append(Comment(line, lines_counter + 1 + number_lines_substracted))
                    lines.pop(lines_counter)  # elimina una ocurrencia
                    number_lines_substracted += 1 # incrementa el numero de lineas eliminadas para tenelo en cuenta  al ahora de renumerar las lineas
                    lines_counter -= 1  #Para seguir tratando la linea siguiente a la linea retirrada por ser un comentario sin codigo
                else:   # si es uan linea de comentarios con codigo
                    comments.append(Comment(line, lines_counter + 1 + number_lines_substracted))
                    if line.__contains__('NO LINE FOUND IN FILE'):
                        del lines[lines_counter]  # elimina una ocurrencia
                    else:
                        lines[lines_counter] = get_line_without_comment(line)
            lines_counter += 1
        if comments:
            comments_file = MyFile(file)
            comments_file.set_comments(comments)
            project.add_file(comments_file)
            write_file(file, lines)
    return project


def restore_comments(project):
    for file in project.get_files():
        lines = read_file(file.get_path())
        if lines:
            print("si file")
        else:
            print("no file")

        if lines is None:
            print('fichero no encontrado', file.get_path())  # Si fichero no encontrado
            orphan_lines = [file.get_path()]
            for comment in file.get_comments():
                orphan_lines.append('   ' + str(comment.line_number) + comment.get_line())
            write_file(project.get_path() + '/comments-orphand.txt', orphan_lines)
        else:  # proceso normal en el que se encuentra el fichero donde poner los comentarios
            for comment in file.get_comments():
                if int(comment.line_number) > len(lines): # Si la linea de comentario esta al final de fichero (no se encontro previamente)
                    if comment.get_line().lstrip().startswith("//"):
                        lines.append(comment.get_line())
                    else:
                        lines.append('//' + comment.get_line())
                elif comment.get_line_without_comment() == '\n':   # Si el comentario no tiene codigo, o sea, esta en una linea nueva
                    lines.insert(int(comment.line_number) - 1, comment.get_line())
                elif lines[int(comment.get_line_number()) - 1] == comment.get_line_without_comment():  # Si encuentra la linea del comentario en su posicion correcta
                    lines[int(comment.get_line_number()) - 1] = comment.get_line()
                elif lines.__contains__(comment.get_line_without_comment()):  # Si no la encuentra por linea pero si por el codigo
                    lines[lines.index(comment.get_line_without_comment())] = comment.get_line()
                else:
                    lines.append(comment.get_line() + " - " + comment.line_number + " NO LINE FOUND IN FILE" + "\n")  # Si no encuentra la linea lo pone al final
                    print(file.get_name() + " - " + comment.get_line() + " - NO LINE FOUND IN FILE" + "\n")
            write_file(file.get_path(), lines)


def obtain_files(ruta_directorio, extension_list):
    java_files = []
    for root, dirs, files in os.walk(ruta_directorio):
        for file in files:
            for extension in extension_list:
                if file.endswith(extension):  #f-String desde 3.6+ (concat eficiente)
                    java_files.append(os.path.join(root, file))
    return java_files

def is_comment_line(linea):
    match = re.search(constants.pattern_todo, linea)  # checks SCT-INST
    if match:
        return True
    return False

def get_line_without_comment(linea):
    return re.sub(constants.pattern_line, '', linea)

def process_unload_comments_into_file(dir):
    java_files = obtain_files(dir, file_types)
    project_comments = Project(dir)

    project = find_and_remove_comments(project_comments, java_files)
    save_project_to_file(project)

def process_load_comments_into_project(file):
    project = load_project_from_file(file)
    restore_comments(project)

    #TODO Incluir un ficherro de configuracion donde se guarde los ultimos ficheros utilizados para no tener que buscar la ruta de nuevo


def seleccionar_opcion_grafica():
    initial_dir_proyects = '/Users/n90578/Library/CloudStorage/OneDrive-SantanderOffice365/Documents/WksPh/501'
    initial_dir_comments = '/Users/n90578/Library/CloudStorage/OneDrive-SantanderOffice365/Documents/WksJavi/PythonGitLab/projects/project1/src/javaComments/output'


    opcion = messagebox.askquestion("Seleccionar Opción",
                                    "Tratar comentarios:\nSelecciona 'Sí' para Retirarlos o 'No' para Cargarlos.")

    if opcion == "yes":
        dir = filedialog.askdirectory(initialdir=initial_dir_proyects, title="Seleccionar directorio")
        if dir:
            process_unload_comments_into_file(dir)
        else:
            print("No se seleccionó ningún directorio.")
    else:
        file = filedialog.askopenfilename(initialdir=initial_dir_comments, title="Selecciona un fichero de comentarios")
        if file:
            process_load_comments_into_project(file)
        else:
            print("No se seleccionó ningún fichero de comentarios.")


if __name__ == "__main__":
    file_types = [".java", ".yml"]

    #TODO esto es para seleccionar un fichero con la interface grafica
    root = tk.Tk()
    root.withdraw()

    seleccionar_opcion_grafica()
    root.quit()


    # dir = filedialog.askdirectory(initialdir=initial_dir, title="Seleccionar directorio")
    # if dir:
    #      print("Processing:", dir)
    #     process(dir)
    # else:
    #     print("No se seleccionó ningún directorio.")

    #TODO esto es para no tener que seleccionar un fichero con la interface grafica
     # process('/Users/n90578/Library/CloudStorage/OneDrive-SantanderOffice365/Documentos/WksJavi/java/Prueba2')

