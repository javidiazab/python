from typing import List
from projects.project1.src.javaComments.src.utils import constants

import os
import re


class Comment:
    line = ""
    line_number = 0

    def __init__(self, line, line_number):
        self.line = line
        self.line_number = line_number

    def get_comment(self):
        return re.search(constants.pattern_line, self.line).group(1)

    def get_line(self):
        return self.line

    def get_line_without_comment(self):
        return re.sub(constants.pattern_line, '', self.line)

    def get_line_number(self):
        return int(self.line_number)


class MyFile:
    path = ""
    comments: List[Comment] = []

    def __init__(self, path):
        self.path = path

    def get_name(self):
        return os.path.basename(self.path)

    def get_path(self):
        return self.path

    def get_comments(self):
        return self.comments

    def set_comments(self, comments):
        self.comments = comments

    def add_comment(self, comment):
        return self.comments.append(comment)


class Project:
    name = ""
    path = ""
    files: List[MyFile] = []

    def __init__(self, path):
        self.path = path
        self.name = os.path.basename(path)

    def get_name(self):
        return self.name

    def get_path(self):
        return self.path

    def get_files(self):
        return self.files

    def add_file(self, file):
        return self.files.append(file)



