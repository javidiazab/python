import javalang

# Define la ruta del archivo Java que quieres analizar
ruta_archivo_java = "Clase2.java"

# Lee el contenido del archivo Java
with open(ruta_archivo_java, "r") as archivo_java:
    codigo_java = archivo_java.read()

# Analiza el código Java y obtiene el árbol sintáctico
arbol_sintactico = javalang.parse.parse(codigo_java)

# Itera sobre los nodos del árbol sintáctico
for nodo, _ in arbol_sintactico:
    # Si el nodo es una clase, interfaz o enum
    if isinstance(nodo, javalang.tree.ClassDeclaration):
        print("Tipo:", nodo)

        # Imprime los campos de la clase
        print("Campos:")
        for campo in nodo.body:
            if isinstance(campo, javalang.tree.FieldDeclaration):
                print("  Nombre:", campo.declarators[0].name)
                print("  Tipo:", campo.type)
                print("  Modificadores:", campo.modifiers)
                print("  Anotaciones:", campo.annotations)
                print()

        # Imprime los métodos de la clase
        print("Métodos:")
        for metodo in nodo.methods:
            print("  Nombre:", metodo.name)
            print("  Tipo de retorno:", metodo.return_type)
            print("  Modificadores:", metodo.modifiers)
            print("  Anotaciones:", metodo.annotations)
            print("  Parámetros:")
            for parametro in metodo.parameters:
                print("    Nombre:", parametro.name)
                print("    Tipo:", parametro.type)
                print("    Modificadores:", parametro.modifiers)
                print("    Anotaciones:", parametro.annotations)
            print()("- Parámetros:", metodo.parameters)