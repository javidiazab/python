### PENDING


3. meter este codigo para detectar los if:
```
import re

def is_java_if_clause(s):
# Regular expression to match a Java if clause
pattern = r'if\s*\((.*)\)\s*\{'
match = re.search(pattern, s)

    if match:
        # Find the positions of the opening and closing parentheses
        start_pos = s.find('(')
        end_pos = s.find(')', start_pos)
        return (start_pos, end_pos)
    else:
        return None

# Example usage
java_if_clause = "if (a > b) {"
result = is_java_if_clause(java_if_clause)
if result:
print(f"Opening parenthesis at: {result[0]}, Closing parenthesis at: {result[1]}")
else:
print("Not a Java if clause")
```