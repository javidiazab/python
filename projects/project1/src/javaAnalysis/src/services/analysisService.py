from projects.project1.src.javaAnalysis.src.components.javaComponents import JavaComponent
from projects.project1.src.javaAnalysis.src.utils.patterns import Patterns
from projects.project1.src.javaAnalysis.src.model.javaClass import JavaClass

import re


patterns = Patterns()
opened_curlybraces = 1
closed_curlybraces = 0
opened_parenthesis = 0
closed_parenthesis = 0


def read_file_lines(file):
    try:
        with open(file, 'r') as file:
            lines = []
            for line in file:
                lines.append(line)
            return lines
    except Exception as e:
        print(e)
    finally:
        file.close()


def init_curlybraces_control():
    opened_curlybraces = 0
    lose_curlybraces = 0


def curlybraces_control(line):
    opened_curlybraces = re.findall(r'{', line).__len__()
    closed_curlybraces = re.findall(r'}', line).__len__()
    return opened_curlybraces - closed_curlybraces


def check_class_completion(line):
    init_curlybraces_control()
    if curlybraces_control(line) == 0:
        return True
    # Controlar que sea menor que 1. Esto es que hay mas llaves de cierre que de apertura y eso no se deberia de dar. Lanzar una excepcion
    return False


def init_parenthesis_control():
    opened_parenthesis = 0
    closed_parenthesis = 0


def parenthesis_control(line):
    opened_parenthesis = re.findall(r'\(', line).__len__()
    closed_parenthesis = re.findall(r'\)', line).__len__()
    return opened_parenthesis - closed_parenthesis


def check_sentence_completion(line):
    init_parenthesis_control()
    if parenthesis_control(line) == 0:
        return True
    return False


def get_name(line):
    return re.match(patterns.get_patterns()[JavaComponent.CLASS_SIMPLE], line).group(1)


def get_type(line):
    return re.match(patterns.get_patterns()[JavaComponent.CLASS_TYPE], line).group(1)


def get_scope(line):
    try:
        return re.match(patterns.get_patterns()[JavaComponent.CLASS_SCOPE], line).group(1)
    except:
        return "package"


def get_extends(line):
    match = re.search(patterns.get_patterns()[JavaComponent.CLASS_EXTENDS], line)
    if match:
        return match.group(1)
    else:
        return ""


def get_implements(line):
    matches = re.findall(patterns.get_patterns()[JavaComponent.CLASS_IMPLEMENTS], line)
    return [imp.strip() for imp in matches[0].split(",")] if matches else None


def get_throws(line):
    matches = re.findall(patterns.get_patterns()[JavaComponent.CLASS_THROWS], line)
    return [imp.strip() for imp in matches[0].split(",")] if matches else None


def get_abstract(line):
    if 'abstract' in line:
        return 'abstract'
    return ""


def get_final(line):
    if 'final' in line:
        return 'final'
    return ""


def process_file(java_file):
    print("opened_curlybraces", opened_curlybraces)
    in_a_class = False
    lines = read_file_lines(java_file.get_file())
    # java_file.set_body(lines)  //cuando se termine se podria meter. mientras hace mucho ruido al consultar la clase a ojo
    pos = 0
    annotations = []
    print("Analizando", java_file.get_class_name())

    try:
        while pos < len(lines):
            line = lines[pos]
            if comment_whole_sentence(line.lstrip()):
                pos += 1
                continue
            if comment_part_sentence(line):
                line = comment_part_sentence(line)
                comment_line = comment_part_comment(line) #esto habra que meterlo algun dia en la sentencia leida
            analise_package(java_file, line)
            analise_imports(java_file, line)
            annotation = analise_annotations(line, lines, pos)
            if annotation:
                annotations.append(annotation)
            analise_classes(java_file, line, annotations, lines, pos)
            pos += 1
    except Exception as a:
        print("Error procesando", java_file, a.__context__)
    finally:
        print("Fin Analisis")


def analise_classes(java_file, line, annotations, lines, pos):
    if class_sentence(line):
        print("Encontrada una clase")
        in_a_class = True
        current_class = JavaClass(get_name(line))
        print(  'Lo que tiene java_file: ', java_file.get_class_name)
        java_file.add_class(current_class)
        current_class.set_annotations(annotations[:]) # pasa una copia. Se crea una copia de annotations
        current_class.set_type(get_type(line))
        current_class.set_scope(get_scope(line))
        current_class.set_abstract(get_abstract(line))
        current_class.set_final(get_final(line))
        current_class.set_extends(get_extends(line))
        current_class.set_implements(get_implements(line)[:]) # pasa una copia
        current_class.set_throws(get_throws(line)[:]) # pasa una copia
        lines_in_class = [line]
        if '{' in line and check_class_completion(line):
            pass  # TODO se abre y cierra la clase en la misma linea
        else:
            while True:
                pos += 1
                if pos >= len(lines):
                    break
                line = line + lines[pos]
                lines_in_class.append(line)
                if check_class_completion(line):
                    break


        array_cadenas = ["apple {{{}", "banana}}"]
        caracter = '{'
        # Usando comprension de listas para encontrar las posiciones de 'a' en cada cadena
        indices = [(i,j) for i, palabra in enumerate(array_cadenas) for j, c in enumerate(palabra) if c == caracter]
        print(indices)

        # current_class.set_body(line)  //esto para que? una linea en el body????
        # process_class(line)  //Tiene dentro la busqueda de anotaciones. esto ya sobra
        annotations = []
        in_a_class = False
    return pos


def analise_annotations(line, lines, pos):
    if annotation_sentence(line):
        print("Encontradas anotaciones_simple", line)
        if check_sentence_completion(line):
            return re.match(patterns.get_patterns()[JavaComponent.ANNOTATIONS_SIMPLE], line).group(1).rstrip()
        else:
            while True:
                pos += 1
                line = line + lines[pos]
                if pos >= len(lines) or check_sentence_completion(line):
                    break
            if re.search(patterns.get_patterns()[JavaComponent.ANNOTATIONS_COMPLEX], line):
                print("Encontradas las anotaciones adicionales", line)
                return line.strip()
    return None

def analise_imports(java_file, line):
    if import_sentence(line):
        print("Encontrado import")
        java_file.add_import(re.match(patterns.get_patterns()[JavaComponent.IMPORTS], line).group(1))


def analise_package(java_file, line):
    if package_sentence(line):
        print("Encontrado el paquete")
        java_file.set_package(re.match(patterns.get_patterns()[JavaComponent.PACKAGE], line).group(1))


def class_sentence(line):
    return re.search(patterns.get_patterns()[JavaComponent.CLASS_SIMPLE], line)


def annotation_sentence(line):
    if line.lstrip().startswith("@"):
        return re.search(patterns.get_patterns()[JavaComponent.ANNOTATIONS_SIMPLE], line)


def import_sentence(line):
    return re.search(patterns.get_patterns()[JavaComponent.IMPORTS], line)


def package_sentence(line):
    return re.search(patterns.get_patterns()[JavaComponent.PACKAGE], line)


def process_class(class_lines):  # De momento quitar
    annotations = analise_annotations(class_lines)
    # print("Processing class", class_lines)


def comment_whole_sentence(line):
    return re.search(patterns.get_patterns()[JavaComponent.COMMENT_TODO], line)

def comment_part_sentence(line):
    match = re.match(patterns.get_patterns()[JavaComponent.COMMENT_PART], line)
    if match:
        return match.group(1).rstrip()


def comment_part_comment(line):
    match = re.search(patterns.get_patterns()[JavaComponent.COMMENT_PART], line)
    if match:
        return match.group(2).lstrip()