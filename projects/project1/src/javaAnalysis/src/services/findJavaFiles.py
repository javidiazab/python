import tkinter as tk
from tkinter import filedialog
import os


def print_java_files(files):
    print("Archivos Java encontrados:")
    for file in files:
        print(file)


def select_java_files(path, extension_list):
    java_files = []
    for root, dirs, files in os.walk(path):
        for file in files:
            for extension in extension_list:
                if file.endswith(extension):  #f-String desde 3.6+ (concat eficiente)
                   java_files.append(os.path.join(root, file))
    return java_files


def ask_4_directory(initial_dir):
    directory = filedialog.askdirectory(initialdir=initial_dir, title="Seleccionar directorio")
    return directory


def ask_4_directory_and_select_java_files(path):
    root = tk.Tk()
    root.withdraw()

    directory = ask_4_directory(path)

    if directory:
        java_files = select_java_files(directory, [".java"])
        print_java_files(java_files)
        return java_files
    else:
        print("No se seleccionó ningún directorio.")
        return None



