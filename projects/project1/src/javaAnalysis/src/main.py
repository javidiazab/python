from typing import List

from projects.project1.src.javaAnalysis.src.services.findJavaFiles import select_java_files
from projects.project1.src.javaAnalysis.src.services.analysisService import process_file
from projects.project1.src.javaAnalysis.src.model.javaFile import JavaFile

path = '/Users/N90578/Documents/WksPh/501/iberpay-inst-gateway/src/main'
# path = '/Users/n90578/Library/CloudStorage/OneDrive-SantanderOffice365/Documents/WksJavi/PythonGitLab/projects/project1/src/javaAnalysis/in'
# path = '/Users/N90578/Documents/WksUK/501Legion/iberpay-inst-gateway/src/main/java/com/pagonxt/paymentshub/iberpayinstgateway/components/await'
# path = '/Users/N90578/Documents/_Javi/Personal/Prueba/java'
java_files: List[JavaFile] = []


def __init__():
    pass


def read_file(file):
    file = open(file, 'r')
    return file.read()


def run():
    # files = ask_4_directory_and_select_java_files(path)
    files = select_java_files(path, [".java"])

    for file in files:
        java_files.append(JavaFile(file))

    for file in java_files:
        process_file(file)

    # for java_file in java_files:
    #     print(java_file)

        # para encontrar una clase en particular
        java_file = next((java_file for java_file in java_files if java_file.get_class_name() == "Event"), None)
        if java_file:
            print("Encontrada GatewayFlag", java_file)

    print("Fin de proceso")


if __name__ == "__main__":
    run()
