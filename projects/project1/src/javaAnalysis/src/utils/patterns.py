from projects.project1.src.javaAnalysis.src.components.javaComponents import JavaComponent


class Patterns:
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
            print("Se ha creado una instancia de Patterns")
        return cls._instance

    patterns = dict()

    # comentarios
    patterns[JavaComponent.COMMENT_TODO] = r'^(\/\/|#).*'
    patterns[JavaComponent.COMMENT_PART] = r'(.*)((\/\/|#).*)'

    # "package com.example.mypackage;"
    patterns[JavaComponent.PACKAGE] = r'^(package\s+\w+(\.\w+)*);$'

    # import static com.example.mypackage.MyClass;
    patterns[JavaComponent.IMPORTS] = r'^(import\s+(?:static\s+)?\w+(?:\.\w+)*(?:\.\*)?);$'

    # private static final int cont;"
    patterns[JavaComponent.ATTRIBUTES] = r'\s*(?:(?:(?:public|private|protected\s+)?(?:static\s+)?(?:final\s+)?)?\w+|val|var)\s+(\w+)\s*;'

    # @Build
    patterns[JavaComponent.ANNOTATIONS_SIMPLE] = r'(@\w+)\s*'
    patterns[JavaComponent.ANNOTATIONS_COMPLEX] = r'((@\w+)\s*(?:\([^()]*\)))?'

    # protected abstract String metodoConArgumentos(String arg1, int arg2) {
    patterns[JavaComponent.METHODS] = r'\s*(?:public|private|protected|package\s+)?(?:abstract\s+)?(?:static\s+)?\w+\s+(\w+)\s*\([^()]*\)\s*\{?'

    # metodo(uno, dos)
    patterns["method2"] = r'(?<!\.)(?:\w+\s*\.\s*)?(\w+)\s*\(?:\s*(?:[^\(\)]*|\([^()]*\))*\)'

    # private static final int contador = Clase.metodo(arg1, arg2)
    assignee_pattern = r'\s*(((?:public|private|protected\s+)?(?:static\s+)?(?:final\s+)?)?\w+|val|var)\s+(\w+)\s*'
    igual = r'='
    assigner_pattern = r'\s*(?:\w+\s*\.\s*)?(\w+)\s*\(\s*(?:[^\(\)]*|\([^()]*\))*\)'
    patterns[JavaComponent.ASIGNATION] = assignee_pattern + igual + assigner_pattern

    # if (nombre != null) {
    patterns[JavaComponent.IF] = r'\s*(if\s*\(([^()]*)\)\s*\{?)'

    # public abstract class xxx {
    patterns[JavaComponent.CLASS_SIMPLE] = r'^\s*(?:(?:public|private|protected|package)\s+)?(?:final\s+)?(?:abstract\s+)?(?:class|enum|interface|record)\s+([A-Z]\w+)'
    patterns[JavaComponent.CLASS_SCOPE] = r'^\s*(?:(public|private|protected|package)\s+)?(?:final\s+)?(?:abstract\s+)?(?:class|enum|interface|record)\s+(?:[A-Z]\w+)'
    patterns[JavaComponent.CLASS_TYPE] = r'^\s*(?:(?:public|private|protected|package)\s+)?(?:final\s+)?(?:abstract\s+)?(class|enum|interface|record)\s+(?:[A-Z]\w+)'
    patterns[JavaComponent.CLASS_COMPLEX] = r'^\s*(?:(?:public|private|protected|package)\s+)?(?:final\s+)?(?:abstract\s+)?(class|enum|interface|record)\s+([A-Z]\w+)(?:\s+extends\s+(\w+)\s*)?(?:implements\s+(\w+)\s*(?:,\s*(\w+))*\s*)?\s*{'
    patterns[JavaComponent.CLASS_EXTENDS] = r'extends\s+([A-Za-z_][A-Za-z0-9_]*)'
    patterns[JavaComponent.CLASS_IMPLEMENTS] = r'implements\s+([^,{\s]+(?:\s*,\s*[^,{\s]+)*)'
    patterns[JavaComponent.CLASS_THROWS] = r'throws\s+([^,{\s]+(?:\s*,\s*[^,{\s]+)*)'


    pattern_class2 = r'\s*(?:public\s+)?(?:abstract\s+)?(class|enum|interface|record)\s+([A-Z]\w+)(?:\s+extends\s+(\w+)\s*)?(?:implements\s+(\w+)\s*(?:,\s*(\w+))*\s*)?\s*{*}'

    def get_patterns(self):
        return self.patterns
