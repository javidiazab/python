from enum import Enum


class JavaComponent(Enum):
    CLASS_SIMPLE = 14
    CLASS_SCOPE = 18
    CLASS_TYPE = 19
    CLASS_EXTENDS = 20
    CLASS_IMPLEMENTS = 21
    CLASS_THROWS = 22
    CLASS_COMPLEX = 16
    ROUTE = 15
    PACKAGE = 1
    IMPORTS = 2
    ANNOTATIONS_SIMPLE = 4
    ANNOTATIONS_COMPLEX = 17
    ATTRIBUTES = 3
    METHODS = 5
    DEPENDENCIES = 6
    DEPENDS = 7
    EXTENDS = 8
    IMPLEMENTS = 9
    THROWS = 10
    ASIGNATION = 11
    BODY = 12
    IF = 13
    COMMENT_TODO = 23
    COMMENT_PART= 24

