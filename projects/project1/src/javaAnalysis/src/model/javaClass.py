import logging


def load_body(file):
    try:
        resource = open(file, 'r')
        content = resource.read()
        resource.close()
        return content
    except:
        return None


class JavaClass:
    class_name = ""
    scope = ""
    abstract = ""
    final = ""
    type = ""
    annotations = []
    attributes = []
    methods = []
    dependencies = []
    depends = []
    extends = ""
    implements = []
    throws = []
    body = ""

    def __init__(self, class_name):
        self.class_name = class_name
        self.scope = None
        self.type = None
        self.abstract = None
        self.final = None
        self.annotations = None
        self.attributes = None
        self.methods = None
        self.dependencies = None
        self.depends = None
        self.extends = None
        self.implements = None
        self.throws = None
        self.body = None

        logging.info('Se acaba de crear una JavaClass {} en {}'.format(self.class_name, self.annotations))
        print("Se acaba de crear la clase {} en {}".format(self.class_name, self.annotations))

    def __str__(self):
        return "Clase: {}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n".format(
        self.class_name,
        self.scope,
        self.type,
        self.abstract,
        self.final,
        self.annotations,
        self.attributes,
        self.methods,
        self.dependencies,
        self.depends,
        self.extends,
        self.implements,
        self.throws,
        self.body,

        )

    def get_class_name(self):
        return self.class_name

    def get_body(self):
        return self.body

    def set_body(self, value):
        self.body = value

    def set_annotations(self, value):
        if isinstance(value, list):
            self.annotations = value
        else:
            raise ValueError("El valor para annotations debe ser una lista")

    def set_type(self, value):
        self.type = value

    def set_scope(self, value):
        self.scope = value

    def set_abstract(self, value):
        self.abstract = value

    def set_final(self, value):
        self.final = value

    def set_extends(self, value):
        self.extends = value

    def set_throws(self, value):
        self.throws.append(value)

    def set_implements(self, value):
        self.implements.append(value)





