class MethodDTO:
    class_name = ""
    method_name = ""
    annotations = []
    params = []
    returns = ""

    def __init__(self, class_name, method_name, params, returns):
        self.class_name = class_name
        self.method_name = method_name
        self.params = params
        self.returns = returns

        print("Se acaba de crear un metodo {}: {}".format(self.class_name, self.method_name))
