import logging
import os
from datetime import datetime
from typing import List

from projects.project1.src.javaAnalysis.src.model.javaClass import JavaClass


def load_body(file):
    try:
        resource = open(file, 'r')
        content = resource.read()
        resource.close()
        return content
    except:
        return None


class JavaFile:
    class_name = ""
    java_file = ""
    updated_time = ""
    java_classes: List[JavaClass] = []
    package = ""
    imports = []
    dependencies = []
    body = ""

    def __init__(self, java_file):
        self.class_name = os.path.basename(java_file).rsplit('.', 1)[0]
        self.java_file = java_file
        self.updated_time = datetime.fromtimestamp(os.path.getmtime(java_file)).strftime('%Y-%m-%d %H:%M:%S')
        self.java_classes = []
        self.package = []
        self.imports = []
        self.dependencies = []
        self.body = ""

    def __str__(self):
        string = ""
        if self.java_classes:
            for java_class in self.java_classes:
                string = str(java_class)
        else:
            string = ""

        return ("File: {}\n{}\n{}\n{}\n{}\n{}\n{}\n{}"
                .format(self.java_file, self.updated_time, self.class_name, string,
                        self.package, self.imports, self.dependencies,
                        self.body, ))

    def get_file(self):
        return self.java_file

    def get_class_name(self):
        return self.class_name

    def set_package(self, value):
        self.package = value

    def add_import(self, import_sentence):
        self.imports.append(import_sentence)

    def add_class(self, value):
        self.java_classes.append(value)

    def get_body(self):
        return self.body

    def set_body(self, value):
        self.body = value
