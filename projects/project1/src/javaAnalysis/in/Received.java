package com.pagonxt.paymentshub.iberpayinstgateway.events;

import static lombok.AccessLevel.PROTECTED;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@EqualsAndHashCode(callSuper = false)
public class Received extends Event<Received.Payload> {

    private static final String CURRENT_VERSION = "1";
    private static final String RECEIVED_EVENT_NAME = "received";

    @Builder
    public Received(String paymentHubId, String initiator, String type, String clientId, String channel, String message,
        String flow, String scheme) {
        super(paymentHubId, RECEIVED_EVENT_NAME, CURRENT_VERSION, clientId, Payload.builder()
                .initiator(initiator)
                .type(type)
                .flow(flow)
                .clientId(clientId)
                .messagePayload(MessagePayload.builder()
                    .channel(channel)
                    .format("XML")
                    .message(message)
                    .build())
                .build(),
            flow, scheme);
    }

    @Getter
    @Builder
    @AllArgsConstructor(access = PROTECTED)
    @NoArgsConstructor(access = PROTECTED)
    public static class Payload {

        private String initiator;
        private String type;
        private String flow;
        private String clientId;
        private MessagePayload messagePayload;
    }

    @Getter
    @Builder
    @AllArgsConstructor(access = PROTECTED)
    @NoArgsConstructor(access = PROTECTED)
    public static class MessagePayload {

        private String channel;
        private String format;
        private String message;
    }
}
