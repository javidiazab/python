import re


def machea(pattern1, cadena1):
    return re.match(pattern1, cadena1, re.MULTILINE)


cadena = "package com.example.mypackage;"
pattern = r'^\s*package\s+\w+(\.\w+)*;$'
print(machea(pattern, cadena))

cadena = "import static com.example.mypackage.MyClass;"
pattern = r'^\s*import\s+(static\s+)?\w+(\.\w+)*(?:\.\*)?;$'
print(machea(pattern, cadena))

cadena = "val cadena;"
cadena2 = "private static final int contador;"
cadena3 = "boolean cadena;"
pattern = r'\s*(?:(?:((?:public|private|protected)\s+)?(?:static\s+)?(?:final\s+)?)?\w+|val|var)\s+(\w+)\s*;'
print(machea(pattern, cadena2))
print(machea(pattern, cadena2).groups())

cadena = "@Build"
pattern = r'@(\w+)\s*(?:\([^()]*\))?'
print(machea(pattern, cadena))
print(machea(pattern, cadena).groups())

cadena = "protected abstract String metodoConArgumentos(String arg1, int arg2) {"
pattern = r'\s*((?:public|private|protected)\s+)?(?:abstract\s+)?(?:static\s+)?\w+\s+(\w+)\s*\([^()]*\)\s*\{?'
print(machea(pattern, cadena))
print(machea(pattern, cadena).groups())

cadena = "metodo(uno, dos)"
cadena2 = "instancia.metodo(uno, dos)"
cadena3 = "Clase.metodo(uno, dos)"
cadena4 = "Clase.metodo(uno + 2, dos)"
cadena5 = "Clase.metodo(uno(uno, dos), dos)"
cadena6 = ".metodo(uno(uno, dos), dos)"  #esta cadea no la debe encontrar
pattern = r'(?<!\.)(?:\w+\s*\.\s*)?(\w+)\s*\(?:\s*(?:[^\(\)]*|\([^()]*\))*\)'
print(machea(pattern, cadena5))
print(machea(pattern, cadena5).groups() if machea(pattern, cadena5) else "")

cadena = "private static final int contador = Clase.metodo(arg1, arg2)"
asignee_pattern = r'\s*((((?:public|private|protected)\s+)?(?:static\s+)?(?:final\s+)?)?\w+|val|var)\s+(\w+)\s*'
igual = r'='
asigner_pattern = r'\s*(?:\w+\s*\.\s*)?(\w+)\s*\(\s*(?:[^\(\)]*|\([^()]*\))*\)'
pattern = asignee_pattern + igual + asigner_pattern
print(machea(pattern, cadena))
print(machea(pattern, cadena).groups())

cadena = "if (nombre != null) {"
pattern = r'\s*if\s*\(([^()]*)\)\s*\{?'
print(machea(pattern, cadena))
print(machea(pattern, cadena).groups())


cadena = "public class Hola {"
cadena2 = "public abstract class Hola extends AnotherClass implements inter1, inter2, inter3 {"
pattern = r'\s*(?:public\s+)?(?:abstract\s+)?(?:class|enum|interface)\s+([A-Z]\w+)(?:\s+extends\s+(\w+)\s*)?(?:implements\s+(\w+)\s*(?:,\s*(\w+))*\s*)?\s*{'
print(machea(pattern, cadena).groups())
print(machea(pattern, cadena2).groups())


cadena = """
public class Hola {\n
    String x = "a";
    public String metodo(String a, String b) {
        return a + b;
    }
}
"""
pattern = r'\s*(?:public\s+)?(?:abstract\s+)?(?:class|enum|interface)\s+([A-Z]\w+)(?:\s+extends\s+(\w+)\s*)?(?:implements\s+(\w+)\s*(?:,\s*(\w+))*\s*)?\s*{*}'
print(machea(pattern, cadena))
