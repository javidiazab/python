package com.pagonxt.paymentshub.iberpayinstgateway.service.common;

import static com.pagonxt.paymentshub.iberpayinstgateway.model.MessageReason.FATAL_EXCEPTION;
import static com.pagonxt.paymentshub.iberpayinstgateway.model.MessageReason.NOT_ACCEPTABLE;
import static com.pagonxt.paymentshub.iberpayinstgateway.model.MessageReason.NOT_PARSEABLE;
import static com.pagonxt.paymentshub.iberpayinstgateway.model.MessageReason.ORIGINAL_NOT_FOUND;
import static com.pagonxt.paymentshub.iberpayinstgateway.model.MessageReason.REQUIRED_INFORMATION;
import static com.pagonxt.paymentshub.iberpayinstgateway.model.MessageReason.SCHEME_REJECTED;
import static com.pagonxt.paymentshub.iberpayinstgateway.model.MessageStatus.CONFIRMED;
import static com.pagonxt.paymentshub.iberpayinstgateway.model.MessageStatus.ERROR;
import static com.pagonxt.paymentshub.iberpayinstgateway.model.MessageStatus.PROCESSED;
import static com.pagonxt.paymentshub.iberpayinstgateway.model.MessageStatus.RECEIVED;
import static com.pagonxt.paymentshub.iberpayinstgateway.model.MessageStatus.REJECTED;
import static com.pagonxt.paymentshub.iberpayinstgateway.model.iberpay.IberpayStatusCode.IBP400;
import static com.pagonxt.paymentshub.iberpayinstgateway.model.iberpay.IberpayStatusCode.IberpayResponse.createResponse;
import static com.pagonxt.paymentshub.iberpayinstgateway.utils.ph.PhMessageUtils.buildReasonInfo;
import static com.pagonxt.paymentshub.iberpayinstgateway.utils.ph.PhMessageUtils.generateStatusReport;
import static com.pagonxt.paymentshub.iberpayinstgateway.utils.ph.PhMessageUtils.getSettlementDate;
import static com.pagonxt.paymentshub.iberpayinstgateway.utils.ph.SupplementaryDataUtils.buildMap;
import static com.pagonxt.paymentshub.sepainstlib.utils.Shortname.camt_056;
import static com.pagonxt.paymentshub.sepainstlib.utils.Shortname.pacs_002;
import static com.pagonxt.paymentshub.sepainstlib.utils.Shortname.pacs_004;
import static com.pagonxt.paymentshub.sepainstlib.utils.Shortname.pacs_008;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static uk.co.santander.iso20022.core.Flow.emission;
import static uk.co.santander.iso20022.core.Flow.reception;
import static uk.co.santander.iso20022.core.TxStatus.ACCC;
import static uk.co.santander.iso20022.core.TxStatus.RJCT;
import static uk.co.santander.iso20022.messaging.routingkey.ProcessStatus.ko;
import static uk.co.santander.iso20022.messaging.routingkey.ProcessStatus.ok;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pagonxt.paymentshub.iberpayinstgateway.components.QueueComponent;
import com.pagonxt.paymentshub.iberpayinstgateway.config.RulebookConfig;
import com.pagonxt.paymentshub.iberpayinstgateway.config.properties.ClientsProperties;
import com.pagonxt.paymentshub.iberpayinstgateway.exceptions.DatabaseMessageNotFoundException;
import com.pagonxt.paymentshub.iberpayinstgateway.message.EventEmitter;
import com.pagonxt.paymentshub.iberpayinstgateway.model.iberpay.IberpayStatusCode;
import com.pagonxt.paymentshub.iberpayinstgateway.model.iberpay.IberpayStatusCode.IberpayResponse;
import com.pagonxt.paymentshub.iberpayinstgateway.model.request.IberpayRequest;
import com.pagonxt.paymentshub.iberpayinstgateway.repository.MessageEntity;
import com.pagonxt.paymentshub.iberpayinstgateway.repository.MessageFilter;
import com.pagonxt.paymentshub.iberpayinstgateway.repository.MessageRepository;
import com.pagonxt.paymentshub.iberpayinstgateway.service.ClientFlowService;
import com.pagonxt.paymentshub.iberpayinstgateway.utils.ComponentUtils;
import com.pagonxt.paymentshub.iberpayinstgateway.utils.IberpayStopWatch;
import com.pagonxt.paymentshub.iberpayinstgateway.utils.ParseUtils;
import com.pagonxt.paymentshub.iberpayinstgateway.utils.iberpay.IberpayStatusReportUtils;
import com.pagonxt.paymentshub.iberpayinstgateway.utils.iberpay.IberpayUtils;
import com.pagonxt.paymentshub.iberpayinstgateway.utils.iberpay.hash.IberpayHashGenerator;
import com.pagonxt.paymentshub.iberpayinstgateway.utils.ph.SupplementaryDataUtils;
import com.pagonxt.paymentshub.sepainstlib.exceptions.NotParseableIberpayMessageException;
import com.pagonxt.paymentshub.sepainstlib.model.SepaInstMessage;
import com.pagonxt.paymentshub.sepainstlib.utils.Shortname;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import uk.co.santander.iso20022.core.PaymentContextHubMessage;
import uk.co.santander.iso20022.core.StatusReportHubMessage;
import uk.co.santander.iso20022.core.TxStatus;
import uk.co.santander.iso20022.core.reports.SchemeReport;
import uk.co.santander.iso20022.core.type.externalcode.InvestigationExecutionConfirmation;
import uk.co.santander.iso20022.messaging.routingkey.ProcessStatus;
import uk.co.santander.isocoreclient.api.IsoCoreApi;

@Slf4j
@Service
@RequiredArgsConstructor
public class StatusReportService {

    private final MessageRepository messageRepository;
    private final MessageService messageService;
    private final IsoCoreApi isoCoreApi;
    private final EventEmitter eventEmitter;
    private final ObjectMapper objectMapper;
    private final ClientFlowService clientFlowService;
    private final QueueComponent queueComponent;
    private final RulebookConfig rulebookConfig;
    private final ClientsProperties clientsProperties;

    private final Set<String> IGNORE_RETURN_EVENT_VALUES = Set.of("hospital", "rr-controller");
    @Value("${app.switch-flow}")
    private List<String> clientSanEs;

    public IberpayResponse launchProcess(IberpayRequest iberpayRequest) {
        val sla = IberpayStopWatch.createStarted();
        val statusMessage = MessageEntity.builder()
            .status(RECEIVED)
            .startTime(sla.getStartedDate())
            .build();

        IberpayResponse iberpayResponse;
        try {
            iberpayResponse = processMessage(statusMessage, iberpayRequest);
        } catch (DatabaseMessageNotFoundException e) {
            log.info("Original message not found: {}", e.getMessage(), e);
            statusMessage.updateStatus(REJECTED, ORIGINAL_NOT_FOUND, ORIGINAL_NOT_FOUND.desc());
            iberpayResponse = generateIberpayResponseWithoutDocument(statusMessage, IberpayStatusCode.IBP204);
        } catch (NotParseableIberpayMessageException e) {
            log.warn("Unexpected activity during parse: {}", e.getMessage(), e);
            statusMessage.updateStatus(ERROR, NOT_PARSEABLE, NOT_PARSEABLE.desc() + ":" + e.getMessage());
            iberpayResponse = createResponse(IBP400);
        } catch (Exception e) {
            log.error("Unexpected exception while processing message: {}", e.getMessage(), e);
            statusMessage.updateStatus(ERROR, FATAL_EXCEPTION, FATAL_EXCEPTION.desc() + ":" + e.getMessage());
            throw e;
        } finally {
            saveMessage(sla, statusMessage);
        }

        return iberpayResponse;
    }

    public IberpayResponse processMessage(MessageEntity message, IberpayRequest iberpayRequest) {
        val iberpayStatusReport = fillInitialFields(iberpayRequest, message);
        log.info("Confirmation received of original msg id {}", message.getExtOriginalMsgId());

        val phMsgId = IberpayHashGenerator.getHash(iberpayStatusReport);
        message.setPhMessageId(phMsgId);

        val originalMessage = findOriginalMessage(iberpayStatusReport, message);

        if (isNull(originalMessage)) {
            log.warn("Original message not found");
            throw new DatabaseMessageNotFoundException("Unable to get an unique message. Found {} candidates");
        }

        fillFieldsFromOriginalMessage(message, originalMessage);

        val paymentHubId = originalMessage.getPaymentHubId();
        message.setPaymentHubId(paymentHubId);

        if (nonNull(paymentHubId)) {
            eventEmitter.emitSchemeResponseEvent(paymentHubId, message.getExtPayload(),
                rulebookConfig.iberpay(pacs_002), message.getClientId());
        }

        if (RECEIVED != originalMessage.getStatus()) {
            log.info("[{}] Received confirmation of null or non-received message with {} current status", paymentHubId,
                originalMessage.getStatus());
            message.updateStatus(REJECTED, NOT_ACCEPTABLE,
                "Original message has not RECEIVED status: " + originalMessage.getStatus());
            return generateIberpayResponseWithoutDocument(message, IberpayStatusCode.IBP200);
        }

        val txStatusOpt = IberpayStatusReportUtils.getTxStatus(iberpayStatusReport);
        if (txStatusOpt.isEmpty()) {
            log.error("[{}] Unable to retrieve status from confirmation message with msg id {}", paymentHubId,
                message.getExtMessageId());
            message.updateStatus(REJECTED, REQUIRED_INFORMATION, REQUIRED_INFORMATION + " : txStatus not found");

            return generateIberpayResponseWithoutDocument(message, IberpayStatusCode.IBP500);
        }

        val statusCode = IberpayStatusReportUtils.getStatusReasonCode(iberpayStatusReport).orElse(null);
        executeConfirmation(originalMessage, txStatusOpt.get(), statusCode, message);

        return generateIberpayResponseWithoutDocument(message, IberpayStatusCode.IBP200);
    }

    public void processConfirmationByPaymentHubId(String paymentHubId, TxStatus txStatus, String statusCode) {
        val originalMessage = findOriginalMessageByPaymentHubId(paymentHubId);
        executeConfirmation(originalMessage, txStatus, statusCode, null);
    }

    public void executeConfirmation(MessageEntity originalMessage, TxStatus txStatus, String statusCode, MessageEntity message) {
        val paymentHubId = originalMessage.getPaymentHubId();

        val originalRequestHubMessage = isoCoreApi.fetchRequestHubMessage(UUID.fromString(paymentHubId));
        val interBankSettlementDate = getSettlementDate(originalRequestHubMessage).orElse(null);

        val confirmedTxStatus = RJCT == txStatus ? RJCT : ACCC;
        val reasonInfo = buildReasonInfo(statusCode);
        val statusReportHubMessage = generateStatusReport(originalRequestHubMessage, confirmedTxStatus, interBankSettlementDate,
            reasonInfo);

        if (nonNull(message)) {
            SupplementaryDataUtils.addSupplementaryData(statusReportHubMessage, buildMap(message));
        } else {
            // For those confirmations from conciliation where there is no pacs002 received
            SupplementaryDataUtils.addSupplementaryData(statusReportHubMessage,
                buildMap(null, originalMessage.getExtMessageId()));
        }

        if (RJCT == txStatus) {
            executeConfirmationRejected(originalMessage, paymentHubId, statusReportHubMessage);
        } else {
            executeConfirmationAccepted(originalMessage, paymentHubId, originalRequestHubMessage, statusReportHubMessage);
        }

        messageRepository.save(originalMessage);
    }

    private void executeConfirmationAccepted(MessageEntity originalMessage, String paymentHubId,
        PaymentContextHubMessage<?> originalRequestHubMessage, StatusReportHubMessage<?> statusReportHubMessage) {
        val processStatus = ProcessStatus.ok;

        originalMessage.setStatus(CONFIRMED);
        originalMessage.setConfirmationDate(LocalDateTime.now());

        switch (Shortname.from(originalMessage.getPhMessageType())) {
            case pacs_008 ->
                paymentConfirmationAccepted(originalMessage, paymentHubId, originalRequestHubMessage, statusReportHubMessage,
                    processStatus);
            case pacs_004 ->
                returnConfirmationAccepted(originalMessage, paymentHubId, originalRequestHubMessage, statusReportHubMessage,
                    processStatus);
            case pacs_028 -> investigationConfirmationAccepted(originalMessage, originalRequestHubMessage);
            default ->
                log.warn("Unrecognized message type executing accepted confirmation: {}", originalMessage.getPhMessageType());
        }
    }

    private void investigationConfirmationAccepted(MessageEntity originalMessage,
        PaymentContextHubMessage<?> originalRequestHubMessage) {
        val newStatusReportHubMessage = originalRequestHubMessage
            .asRequest()
            .statusReportFactory()
            .create(SchemeReport.builder().txStatus(ACCC)
                .investigationStatus(InvestigationExecutionConfirmation.CNCL).build());

        val routingKey = ComponentUtils.buildRoutingKeyByClient(originalMessage.getClientId(), newStatusReportHubMessage, ok,
            clientsProperties);
        queueComponent.sendToExecutionComplete(originalMessage.getPaymentHubId(), newStatusReportHubMessage, routingKey);
    }

    private void returnConfirmationAccepted(MessageEntity originalMessage, String paymentHubId,
        PaymentContextHubMessage<?> originalRequestHubMessage,
        StatusReportHubMessage<?> statusReportHubMessage, ProcessStatus processStatus) {

        if (!IGNORE_RETURN_EVENT_VALUES.contains(originalRequestHubMessage.client().getInitiator())) {

            // Find original message
            if (clientSanEs.contains(originalMessage.getClientId())) {
                val routingKeyToAccountant = ComponentUtils.buildRoutingKeyByClient(originalMessage.getClientId(),
                    statusReportHubMessage,
                    processStatus, clientsProperties);
                queueComponent.sendToQueue(paymentHubId, statusReportHubMessage, routingKeyToAccountant);
            }

            val originalRecall = messageService.find(MessageFilter.builder()
                .originalReferenceId(originalMessage.getOriginalReferenceId())
                .referenceAgent(originalMessage.getReferenceAgent())
                .messageType(camt_056)
                .flow(reception)
                .status(PROCESSED)
                .build());

            originalRecall.setStatus(CONFIRMED);
            messageRepository.save(originalRecall);
        }
    }

    private void paymentConfirmationAccepted(MessageEntity originalMessage, String paymentHubId,
        PaymentContextHubMessage<?> originalRequestHubMessage, StatusReportHubMessage<?> statusReportHubMessage,
        ProcessStatus processStatus) {

        if (reception == originalMessage.getFlow() || clientSanEs.contains(originalMessage.getClientId())) {
            if (emission == originalMessage.getFlow() && clientSanEs.contains(originalMessage.getClientId())) {
                val routingKey = ComponentUtils.buildRoutingKeyByClient(originalMessage.getClientId(),
                    statusReportHubMessage, processStatus, clientsProperties);
                queueComponent.sendToQueue(paymentHubId, statusReportHubMessage, routingKey);
            } else {
                val routingKey = ComponentUtils.buildRoutingKeyByClient(originalMessage.getClientId(),
                    originalRequestHubMessage, processStatus, clientsProperties);
                queueComponent.sendToQueue(paymentHubId, originalRequestHubMessage, routingKey);
            }
        }
    }

    private void executeConfirmationRejected(MessageEntity originalMessage, String paymentHubId,
        StatusReportHubMessage<?> statusReportHubMessage) {
        log.info("[{}] Received rejection from Iberpay and notifying to CORE", paymentHubId);
        originalMessage.updateStatus(REJECTED, SCHEME_REJECTED, SCHEME_REJECTED.desc());

        val routingKey = ComponentUtils.buildRoutingKeyByClient(originalMessage.getClientId(), statusReportHubMessage, ko,
            clientsProperties);
        if (reception == originalMessage.getFlow() || clientSanEs.contains(originalMessage.getClientId())) {
            queueComponent.sendToExecutionComplete(paymentHubId, statusReportHubMessage, routingKey);
        }
    }

    private void fillFieldsFromOriginalMessage(MessageEntity message, MessageEntity originalMessage) {
        message.setFlow(originalMessage.getFlow());
        message.setClientId(originalMessage.getClientId());
        message.setOriginalReferenceId(originalMessage.getReferenceId());

        message.setPaymentHubId(originalMessage.getPaymentHubId());
        message.setPhOriginalMsgId(originalMessage.getPhOriginalMsgId());
        message.setPhOriginalInstructionId(originalMessage.getPhOriginalInstructionId());
    }

    private MessageEntity findOriginalMessageByPaymentHubId(String paymentHubId) {
        return messageService.find(MessageFilter.builder()
            .paymentHubId(paymentHubId)
            .messageTypeList(List.of(pacs_008, pacs_004))
            .status(RECEIVED)
            .build());
    }

    private MessageEntity findOriginalMessage(SepaInstMessage<?> iberpayStatusReport, MessageEntity message) {
        // If we receive a pacs002 confirmation of a pacs004 then it's  inbound return flow
        // So pacs004 ext message id should be unique due to own generation (instance + timestamp)
        val originalMessageDefinition = IberpayStatusReportUtils.getGroupOriginalMessageDefinition(iberpayStatusReport)
            .orElse(null);
        if (nonNull(originalMessageDefinition) && pacs_004 == Shortname.from(originalMessageDefinition)) {
            val filter = MessageFilter.builder()
                .extMessageId(message.getExtOriginalMsgId())
                .referenceId(message.getOriginalReferenceId())
                .referenceAgent(message.getReferenceAgent())
                .status(RECEIVED)
                .flow(emission)
                .build();
            val rejectedFilter = MessageFilter.builder()
                .extMessageId(message.getExtOriginalMsgId())
                .referenceId(message.getOriginalReferenceId())
                .referenceAgent(message.getReferenceAgent())
                .statusNot(RECEIVED)
                .flow(emission)
                .build();
            return findMessage(filter, rejectedFilter);
        }

        val instructedAgentBic = IberpayStatusReportUtils.getInstructedAgent(iberpayStatusReport).orElse(null);

        val filter = MessageFilter.builder()
            .referenceId(message.getOriginalReferenceId())
            .referenceAgent(message.getReferenceAgent())
            .extMessageId(message.getExtOriginalMsgId())
            .confirmationInstructedAgent(instructedAgentBic)
            .status(RECEIVED)
            .build();
        val baseResult = findAndGetMessage(filter);
        if (baseResult.isPresent()) {
            return baseResult.get();
        }

        val expectedClientId = getClientId(instructedAgentBic)
            .orElseThrow(
                () -> new IllegalArgumentException("Unable to retrieve a valid client id from given bic " + instructedAgentBic));
        log.info("[TxId {} - MsgId {}] Cannot retrieve original message with instructed agent bic, apply search with clientId {}",
            message.getOriginalReferenceId(), message.getExtMessageId(), expectedClientId);
        val filterWithClientId = MessageFilter.builder()
            .referenceId(message.getOriginalReferenceId())
            .referenceAgent(message.getReferenceAgent())
            .extMessageId(message.getExtOriginalMsgId())
            .clientId(expectedClientId)
            .status(RECEIVED)
            .build();

        val messageWithClientId = findAndGetMessage(filterWithClientId);
        if (messageWithClientId.isPresent()) {
            return messageWithClientId.get();
        }

        val rejectedFilter = MessageFilter.builder()
            .referenceId(message.getOriginalReferenceId())
            .referenceAgent(message.getReferenceAgent())
            .extMessageId(message.getExtOriginalMsgId())
            .confirmationInstructedAgent(instructedAgentBic)
            .statusNot(RECEIVED)
            .build();
        val messageRejected = findAndGetMessage(rejectedFilter);
        if (messageRejected.isPresent()) {
            return messageRejected.get();
        }

        val rejectedFilterWithClientId = MessageFilter.builder()
            .referenceId(message.getOriginalReferenceId())
            .referenceAgent(message.getReferenceAgent())
            .extMessageId(message.getExtOriginalMsgId())
            .clientId(expectedClientId)
            .statusNot(RECEIVED)
            .build();

        val messageRejectedWithClientId = messageService.findAll(rejectedFilterWithClientId, Pageable.ofSize(1));
        val totalRejectedWithClientId = messageRejectedWithClientId.getTotalElements();
        if (totalRejectedWithClientId == 1L) {
            return messageRejectedWithClientId.getContent().get(0);
        } else {
            throw new DatabaseMessageNotFoundException(
                "Unable to get an unique received message during searchs. Found " + totalRejectedWithClientId + " candidates");
        }
    }

    private Optional<MessageEntity> findAndGetMessage(MessageFilter filter) {
        val messageEntities = messageService.findAll(filter, Pageable.ofSize(1));
        val totalElements = messageEntities.getTotalElements();

        if (totalElements == 1L) {
            return Optional.of(messageEntities.getContent().get(0));
        } else {
            log.info("Unable to retrieve unique value. Found {} candidates", totalElements);
            return Optional.empty();
        }
    }

    private Optional<String> getClientId(String instructedAgentBic) {
        val bic = Optional.of(instructedAgentBic)
            .map(ParseUtils::bic8)
            .orElseThrow(
                () -> new IllegalArgumentException("Unable to obtain a valid bic from given value " + instructedAgentBic));

        return clientFlowService.getClientIdByBic(bic);
    }

    private MessageEntity findMessage(MessageFilter filter, MessageFilter rejectedFilter) {
        val messageEntities = messageService.findAll(filter, Pageable.ofSize(1));
        val totalElements = messageEntities.getTotalElements();

        return switch ((int) totalElements) {
            case 1 -> messageEntities.getContent().get(0);
            case 0 -> findRejectedMessage(rejectedFilter);
            default -> throw new DatabaseMessageNotFoundException(
                "Unable to get an unique received message. Found " + totalElements + " candidates");
        };
    }

    private MessageEntity findRejectedMessage(MessageFilter filter) {
        val rejectedSearch = messageService.findAll(filter, Pageable.ofSize(1));

        if (rejectedSearch.isEmpty()) {
            log.warn("Unable retrieve original message even looking for non-RECEIVED status");
            throw new DatabaseMessageNotFoundException(
                "Unable to get an unique message. Found " + rejectedSearch.getTotalElements() + " candidates");
        }

        log.info("Got {} results in a non-RECEIVED search", rejectedSearch.getTotalElements());

        return rejectedSearch.getContent().get(0);
    }

    private SepaInstMessage<?> fillInitialFields(IberpayRequest iberpayRequest, MessageEntity message) {
        val decodedDocument = new String(iberpayRequest.getDocument(), StandardCharsets.UTF_8);
        message.setExtPayload(decodedDocument);

        val iberpayStatusReport = SepaInstMessage.fromXml(decodedDocument);

        message.setExtOriginalMsgId(IberpayStatusReportUtils.getOriginalMsgId(iberpayStatusReport).orElse(null));
        message.setMessageType(pacs_002);
        message.setExtMessageType(iberpayStatusReport.definition());
        message.setExtMessageId(IberpayUtils.getMessageId(iberpayStatusReport).orElse(null));
        message.setExtOriginalInstructionId(IberpayUtils.getInstructionId(iberpayStatusReport).orElse(null));

        // common fields
        message.setReferenceId(IberpayStatusReportUtils.getReferenceId(iberpayStatusReport).orElse(null));
        message.setOriginalReferenceId(IberpayUtils.getTransactionId(iberpayStatusReport).orElse(null));
        message.setReferenceAgent(IberpayUtils.getDebtorBic(iberpayStatusReport).orElse(null));

        return iberpayStatusReport;
    }

    private IberpayResponse generateIberpayResponseWithoutDocument(MessageEntity message, IberpayStatusCode iberpayStatusCode) {
        val iberpayResponse = IberpayResponse.builder().status(iberpayStatusCode).build();

        val paymentHubId = message.getPaymentHubId();
        if (nonNull(paymentHubId)) {
            emitNotifiedResponseEvent(paymentHubId, iberpayResponse, message.getClientId());
        }

        return iberpayResponse;
    }

    private void emitNotifiedResponseEvent(String paymentHubId, IberpayResponse iberpayResponse, String clientId) {
        try {
            val iberpayResponseAsString = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(iberpayResponse);

            eventEmitter.emitNotifiedResponseEvent(paymentHubId, iberpayResponseAsString,
                rulebookConfig.iberpay(pacs_002).schema(), clientId);
        } catch (JsonProcessingException e) {
            log.error("[{}] Unable to deserialize while sending response to iberpay {}", paymentHubId, e.getMessage());
        }
    }

    private void saveMessage(IberpayStopWatch sla, MessageEntity message) {
        sla.stop();
        message.setEndTime(sla.getEndDate());
        messageRepository.save(message);
    }
}
