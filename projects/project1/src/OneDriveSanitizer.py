import tkinter as tk
from tkinter import filedialog
import os


def obtener_archivos_java(ruta_directorio):
    archivos_java_found = []
    for raiz, directorios, archivos in os.walk(ruta_directorio):
        for archivo in archivos:
            if str(archivo).startswith(' '):
                print("removing spaces in", os.path.join(raiz, archivo))
                archivos_java_found.append(os.path.join(raiz, archivo))
                os.rename(os.path.join(raiz, archivo), os.path.join(raiz, str(archivo).strip()))
            if ':' in str(archivo):
                print("removing : in", os.path.join(raiz, archivo))
                archivos_java_found.append(os.path.join(raiz, archivo))
                os.rename(os.path.join(raiz, archivo), os.path.join(raiz, str(archivo).replace(':', '.')))
        for directorio in directorios:
            if '~' == directorio:
                print("removing dir ~ in", os.path.join(raiz, directorio))
                archivos_java_found.append(os.path.join(raiz, directorio))
                os.rename(os.path.join(raiz, directorio), os.path.join(raiz, 'virgalilla_renombrada'))
    return archivos_java_found


root = tk.Tk()
root.withdraw()

# directorio = filedialog.askdirectory(title="Seleccionar directorio")

archivos_java = obtener_archivos_java('/Users/N90578/Documents/_Javi/Personal/Prueba')
# archivos_java = obtener_archivos_java('/Users/N90578/Documents')
# archivos_java = obtener_archivos_java('/Users/N90578/Library/CloudStorage/OneDrive-SantanderOffice365')

print("Archivos Java encontrados:")
for archivo_java in archivos_java:
    print(archivo_java)